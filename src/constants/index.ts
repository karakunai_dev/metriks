export const ACTION = {
    STORE: {
        INIT: "action-store-init",
        CURRENT: "action-store-current",
        SAVED: "action-store-saved",
        VISUAL: "action-store-visual",
    },
}

export const ACCESSKEY = {
    DIALOG: {
        FILE: "q",
        TREATS: "w",
        SETTINGS: "e",
        ABOUT: "r",
    },
}
