import { h } from "preact"
import { Meta, Story } from "@storybook/preact"
import TreatsDialog, { Props } from "./"

export default {
    title: "Metriks/TreatsDialog",
    component: TreatsDialog,
} as Meta<typeof TreatsDialog>

const Template: Story<Props> = (args) => <TreatsDialog {...args} />

export const Default = Template.bind({})
Default.args = {}
