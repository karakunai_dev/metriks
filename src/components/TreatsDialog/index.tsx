import { h, JSX, Fragment } from "preact"
import { useContext, useState } from "preact/hooks"
import PropTypes, { InferProps } from "prop-types"
import { StoreContext } from "../../context"
import { ACTION, ACCESSKEY } from "../../constants"
import Dialog from "../Dialog"
import { matrix, explore, split, backtrack, Pair } from "@ronanharris09/treats"

const propTypes = {}

export type Props = InferProps<typeof propTypes>

const TreatsDialog = (): JSX.Element => {
    const { store, dispatch } = useContext(StoreContext)
    const [initialNode, setInitialNode] = useState<string>("")
    const [destinationNode, setDestinationNode] = useState<string>("")
    const [treatsResult, setTreatsResult] = useState<Array<Array<Pair>>>([])

    const dialogStateHandler = () => {
        let prevState = store
        prevState.visual.dialog.treats = !prevState.visual.dialog.treats

        dispatch({
            type: ACTION.STORE.VISUAL,
            body: prevState,
        })
    }

    const runTreats = () => {
        const pairs = matrix(
            store.current.data,
            store.current.data.map((item, index) => `${index + 1}`)
        )
        const explored = explore(pairs, initialNode, destinationNode)
        const preprocess = split(explored, initialNode, destinationNode)
        const routes = backtrack(preprocess)
        setTreatsResult(routes)
    }

    return (
        <>
            <Dialog
                title="TreaTS"
                description="Process input graph into routes"
                isOpen={store.visual.dialog.treats}
                callback={() => dialogStateHandler()}
                accessKey={ACCESSKEY.DIALOG.TREATS}
            >
                <>
                    <ul class="w-[420px]">
                        <li class="flex flex-col p-4">
                            <label
                                class="font-bold mb-2"
                                for="dialog-treats-intial"
                            >
                                Initial Node
                            </label>
                            <input
                                class="border-2 border-gray-300 p-4"
                                type="text"
                                name="dialog-treats-initial"
                                placeholder="Inset index for initial node"
                                value={initialNode}
                                onInput={(e) =>
                                    setInitialNode(e.currentTarget.value)
                                }
                                autocomplete="off"
                            />
                        </li>
                        <li class="flex flex-col p-4">
                            <label
                                class="font-bold mb-2"
                                for="dialog-treats-destination"
                            >
                                Destination Node
                            </label>
                            <input
                                class="border-2 border-gray-300 p-4"
                                type="text"
                                name="dialog-treats-destination"
                                placeholder="Insert index for destination node"
                                value={destinationNode}
                                onInput={(e) =>
                                    setDestinationNode(e.currentTarget.value)
                                }
                                autocomplete="off"
                            />
                        </li>
                        <li class="flex flex-col p-4">
                            <button
                                type="button"
                                class="p-4 font-bold border-t-2 border-gray-700 outline-none text-white bg-gray-700 hover:text-gray-900 hover:bg-gray-400 focus:text-gray-900 focus:bg-gray-400"
                                onClick={() => runTreats()}
                            >
                                Run TreaTS
                            </button>
                        </li>
                        <li class="flex flex-col p-4">
                            <label
                                class="font-bold mb-2"
                                for="dialog-treats-result"
                            >
                                Result
                            </label>
                            <div
                                class="flex flex-col border-2 border-gray-300 p-4"
                                name="dialog-treats-result"
                                placeholder="TreaTS output goes here..."
                                rows={6}
                                readonly
                            >
                                {treatsResult.map((item, index) => (
                                    <span
                                        key={`DIALOG-TREATS-RESULT-PARENT-${index}`}
                                    >
                                        {item.map((subitem, subindex) => (
                                            <span
                                                key={`DIALOG-TREATS-RESULT-CHILD-${subindex}`}
                                            >
                                                {`${subitem.current} -> `}
                                            </span>
                                        ))}
                                        <span>{destinationNode}</span>
                                    </span>
                                ))}
                            </div>
                        </li>
                    </ul>
                </>
            </Dialog>
        </>
    )
}

TreatsDialog.propTypes = propTypes

TreatsDialog.defaultProps = {}

export default TreatsDialog
