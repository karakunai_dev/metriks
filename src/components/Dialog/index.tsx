import { h, JSX, Fragment } from "preact"
import PropTypes, { InferProps } from "prop-types"
import { useRef, useEffect } from "preact/hooks"

const propTypes = {
    isOpen: PropTypes.bool.isRequired,
    children: PropTypes.element.isRequired,
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    callback: PropTypes.func.isRequired,
    accessKey: PropTypes.string.isRequired,
}

export type Props = InferProps<typeof propTypes>

const Dialog = ({
    isOpen,
    children,
    title,
    description,
    callback,
    accessKey,
}: Props): JSX.Element => {
    const closeFocus = useRef<HTMLButtonElement>(null)

    useEffect(() => {
        if (isOpen) {
            closeFocus.current && closeFocus.current.focus()
        }
    }, [isOpen])

    return (
        <>
            <div
                class={[
                    "fixed inset-0 transition-all duration-300",
                    isOpen ? "block" : "hidden",
                ].join(" ")}
            >
                <div class="relative w-full h-full flex items-center">
                    <div
                        class="absolute w-full h-full backdrop-blur-sm bg-white/30"
                        onClick={() => callback()}
                    ></div>
                    <dialog class="absoulte bg-white shadow-2xl" open={isOpen}>
                        <header class="flex justify-between p-3">
                            <div class="mr-6">
                                <h1 class="text-3xl font-light">{title}</h1>
                                <p>{description}</p>
                            </div>
                            <div class="my-auto">
                                <button
                                    type="button"
                                    class="p-3 text-center text-2xl border-2 border-black bg-white text-black hover:bg-black hover:text-white focus:bg-black focus:text-white focus:outline-none"
                                    onClick={() => callback()}
                                    ref={closeFocus}
                                    accessKey={accessKey}
                                >
                                    ✕
                                </button>
                            </div>
                        </header>
                        <section class="p-3">{children}</section>
                    </dialog>
                </div>
            </div>
        </>
    )
}

Dialog.propTypes = propTypes

Dialog.defaultProps = {
    isOpen: false,
    children: <>This is a fallback children</>,
    title: "Karakunai",
    description: "Lorem ipsum dolor sit amet consectetur adispicing elit",
    callback: (b: boolean) => console.log("DIALOG-VISIBLE", b),
    accessKey: "p",
}

export default Dialog
