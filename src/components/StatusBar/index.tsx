import { h, JSX, Fragment } from "preact"
import { memo } from "preact/compat"
import { useState, useContext } from "preact/hooks"
import PropTypes, { InferProps } from "prop-types"
import { StoreContext } from "../../context"
import { ACTION } from "../../constants"

const propTypes = {
    manualSaveCallback: PropTypes.func.isRequired,
}

export type Props = InferProps<typeof propTypes>

const StatusBar = ({ manualSaveCallback }: Props): JSX.Element => {
    const { store, dispatch } = useContext(StoreContext)

    const addNode = () => {
        let prevStore = store.current
        prevStore.data.map((item) => item.push(0))
        prevStore.data.push(
            Array<number>(prevStore.data.length + 1)
                .fill(0)
                .map((item, index) =>
                    index === prevStore.data.length ? -1 : 0
                )
        )
        prevStore.date = new Date().toISOString()

        dispatch({
            type: ACTION.STORE.CURRENT,
            body: { ...store, current: prevStore },
        })
    }

    const removeNode = () => {
        let prevStore = store.current
        prevStore.data.map((item) => item.splice(prevStore.data.length - 1))
        prevStore.data = prevStore.data.slice(0, prevStore.data.length - 1)
        prevStore.date = new Date().toISOString()

        dispatch({
            type: ACTION.STORE.CURRENT,
            body: { ...store, current: prevStore },
        })
    }

    const resetSheet = () => {
        let prevStore = store.current

        prevStore.data = Array<Array<number>>(prevStore.data.length)
            .fill(Array<number>(prevStore.data.length).fill(0))
            .map((item, index) =>
                item.map((subitem, subindex) => (index === subindex ? -1 : 0))
            )
        prevStore.date = new Date().toISOString()

        dispatch({
            type: ACTION.STORE.CURRENT,
            body: { ...store, current: prevStore },
        })
    }

    const toggleMode = () => {
        let prevStore = store

        switch (prevStore.visual.status.metriks) {
            case "INSERT":
                prevStore.visual.status.metriks = "WRITE"
                break
            case "WRITE":
                prevStore.visual.status.metriks = "INSERT"
                break

            default:
                prevStore.visual.status.metriks = "INSERT"
                break
        }

        dispatch({
            type: ACTION.STORE.VISUAL,
            body: prevStore,
        })
    }

    return (
        <>
            <footer class="w-full bg-gray-100 flex justify-between shadow">
                <ul class="grid grid-flow-col auto-cols-max">
                    <li>
                        <button
                            type="button"
                            class="text-black p-4 border-t-2 border-green-100 bg-green-100 hover:border-green-900 hover:bg-green-200 focus:border-green-900 focus:bg-green-200 active:bg-green-400"
                            onClick={() => addNode()}
                        >
                            Add Node
                        </button>
                    </li>
                    <li>
                        <button
                            type="button"
                            class="text-black p-4 border-t-2 border-red-100 bg-red-100 hover:border-red-900 hover:bg-red-200 focus:border-red-900 focus:bg-red-200 active:bg-red-400"
                            onClick={() => removeNode()}
                        >
                            Remove Node
                        </button>
                    </li>
                    <li>
                        <button
                            type="button"
                            class="text-black p-4 border-t-2 border-yellow-100 bg-yellow-100 hover:border-yellow-900 hover:bg-yellow-200 focus:border-yellow-900 focus:bg-yellow-200 active:bg-yellow-400"
                            onClick={() => resetSheet()}
                        >
                            Reset Sheet
                        </button>
                    </li>
                    <li>
                        <button
                            type="button"
                            class="text-black p-4 border-t-2 border-blue-100 bg-blue-100 hover:border-blue-900 hover:bg-blue-200 focus:border-blue-900 focus:bg-blue-200 active:bg-blue-400"
                            onClick={() => toggleMode()}
                        >
                            Toggle Mode
                        </button>
                    </li>
                    <li>
                        <button
                            type="button"
                            class="text-black p-4 border-t-2 border-violet-100 bg-violet-100 hover:border-violet-900 hover:bg-violet-200 focus:border-violet-900 focus:bg-violet-200 active:bg-violet-400"
                            onClick={() => manualSaveCallback()}
                        >
                            Manual Save
                        </button>
                    </li>
                </ul>
                <ul class="grid grid-flow-col auto-cols-max font-bold">
                    <li class="p-4">Last Write: {store.current.date}</li>
                    <li
                        class={[
                            "p-4",
                            store.visual.status.metriks === "INSERT"
                                ? "bg-teal-200"
                                : "bg-rose-200",
                        ].join(" ")}
                    >
                        {store.visual.status.metriks}
                    </li>
                    <li
                        class={[
                            "p-4 underline decoration-dotted decoration-2",
                            store.current.data.length > 36
                                ? "bg-red-300"
                                : store.current.data.length > 24
                                ? "bg-yellow-300"
                                : "bg-green-300",
                        ].join(" ")}
                    >
                        {`${store.current.data.length}N`}
                    </li>
                    <li class="p-4 bg-black text-white">
                        {store.current.name.length > 24
                            ? `${store.current.name.slice(0, 15)}...`
                            : store.current.name}
                    </li>
                </ul>
            </footer>
        </>
    )
}

StatusBar.propTypes = propTypes

StatusBar.defaultProps = {
    manualSaveCallback: () => {},
}

const memoCompare = (prevState: Props, nextState: Props): boolean => {
    return prevState.manualSaveCallback === nextState.manualSaveCallback
}

export default memo(StatusBar, memoCompare)
