import { h, JSX, Fragment } from "preact"
import { useState, useContext } from "preact/hooks"
import PropTypes, { InferProps } from "prop-types"
import { StoreContext } from "../../context"
import { ACTION } from "../../constants"

const propTypes = {}

export type Props = InferProps<typeof propTypes>

const NavigationBar = (): JSX.Element => {
    const { store, dispatch } = useContext(StoreContext)

    const dialogStateHandler = (menu: string) => {
        let prevState = store

        switch (menu) {
            case "file":
                prevState.visual.dialog.file = true
                break
            case "treats":
                prevState.visual.dialog.treats = true
                break
            case "settings":
                prevState.visual.dialog.settings = true
                break
            case "about":
                prevState.visual.dialog.about = true
                break

            default:
                throw new Error()
        }

        dispatch({
            type: ACTION.STORE.VISUAL,
            body: prevState,
        })
    }

    return (
        <>
            <header class="flex shadow-md bg-white p-4 fixed inset-x-0 top-0">
                <nav class="my-auto">
                    <ul class="grid grid-flow-col auto-cols-max gap-6">
                        <li>
                            <button
                                type="button"
                                class="p-4 border-b-4 border-white focus:border-black hover:border-black font-medium"
                                onClick={() => dialogStateHandler("file")}
                            >
                                File
                            </button>
                        </li>
                        <li>
                            <button
                                type="button"
                                class="p-4 border-b-4 border-white focus:border-black hover:border-black font-medium"
                                onClick={() => dialogStateHandler("treats")}
                            >
                                Run TreaTS
                            </button>
                        </li>
                        <li>
                            <button
                                type="button"
                                class="p-4 border-b-4 border-white focus:border-black hover:border-black font-medium"
                                onClick={() => dialogStateHandler("settings")}
                            >
                                Settings
                            </button>
                        </li>
                        <li>
                            <button
                                type="button"
                                class="p-4 border-b-4 border-white focus:border-black hover:border-black font-medium"
                                onClick={() => dialogStateHandler("about")}
                            >
                                About
                            </button>
                        </li>
                    </ul>
                </nav>
            </header>
        </>
    )
}

NavigationBar.propTypes = propTypes

NavigationBar.defaultProps = {}

export default NavigationBar
