import { h, JSX, Fragment } from "preact"
import { useCallback } from "preact/hooks"
import { memo } from "preact/compat"
import PropTypes, { InferProps } from "prop-types"
import Cell from "../Cell"

const propTypes = {
    rowHover: PropTypes.func.isRequired,
    rowCallback: PropTypes.func.isRequired,
    rowData: PropTypes.arrayOf(PropTypes.number.isRequired).isRequired,
    rowId: PropTypes.number.isRequired,
    renderTrigger: PropTypes.string.isRequired,
}

export type Props = InferProps<typeof propTypes>

const Column = ({
    rowHover,
    rowCallback,
    rowData,
    rowId,
}: Props): JSX.Element => {
    const onCallback = useCallback(
        (d: Array<number>, i: number) => rowCallback(d, i),
        [rowCallback]
    )

    const onHover = useCallback((i: number) => rowHover(i), [rowHover])

    const columnStateHandler = (current: number, index: number) => {
        let prevState = rowData
        prevState[index] = current
        onCallback(prevState, rowId)
    }

    return (
        <>
            <div class="flex">
                {rowData.map((item, index) => (
                    <Cell
                        size="single"
                        key={`Cell-${rowId}-${index}`}
                        current={item}
                        identifier={index}
                        isDisabled={rowId === index}
                        callback={(num) => columnStateHandler(num, index)}
                        isHover={onHover}
                    />
                ))}
            </div>
        </>
    )
}

Column.propTypes = propTypes

Column.defaultProps = {
    rowHover: (i: number) => console.log("Hover", i),
    rowCallback: (e: number, n: number) => console.log(e, n),
    rowData: Array(3).fill(0),
    rouwId: -1,
    renderTrigger: "",
}

const memoCompare = (prevState: Props, nextState: Props): boolean => {
    return (
        prevState.rowData === nextState.rowData &&
        prevState.rowId === nextState.rowId &&
        prevState.renderTrigger === nextState.renderTrigger
    )
}

export default memo(Column, memoCompare)
