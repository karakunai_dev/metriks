import { h } from "preact"
import { useContext } from "preact/hooks"
import { StoreProps, StoreContext } from "../context"

export const useStore = () => {
    const { store, dispatch } = useContext(StoreContext)
    return { store, dispatch }
}
