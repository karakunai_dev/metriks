import {
    h,
    createContext,
    FunctionalComponent,
    ComponentChild,
    ComponentChildren,
} from "preact"
import { useState, useEffect, useReducer } from "preact/hooks"
import { ACTION } from "../constants"
import { sjs, attr } from "slow-json-stringify"

export type MatrixProps = {
    name: string
    data: Array<Array<number>>
    date: string
}

export type UserInterfaceProps = {
    dialog: {
        file: boolean
        treats: boolean
        settings: boolean
        about: boolean
    }
    status: {
        metriks: "INSERT" | "WRITE"
    }
}

export type StoreProps = {
    current: MatrixProps
    saved: Array<MatrixProps>
    visual: UserInterfaceProps
}

export type StoreAction = {
    type: string
    body: Record<string, any>
}

export const StorePropsSchema = sjs({
    current: {
        name: attr("string"),
        data: attr("array"),
        date: attr("string"),
    },
    saved: attr(
        "array",
        sjs({
            name: attr("string"),
            data: attr("array"),
            date: attr("string"),
        })
    ),
    visual: {
        dialog: {
            file: attr("boolean"),
            treats: attr("boolean"),
            setttings: attr("boolean"),
            about: attr("boolean"),
        },
        status: {
            metriks: attr("string"),
        },
    },
})

const initialState: StoreProps = {
    current: {
        name: "Untitled",
        data: [],
        date: new Date().toISOString(),
    },
    saved: [
        {
            name: "Empty Sheet (7N)",
            data: [
                [-1, 0, 0, 0, 0, 0, 0],
                [0, -1, 0, 0, 0, 0, 0],
                [0, 0, -1, 0, 0, 0, 0],
                [0, 0, 0, -1, 0, 0, 0],
                [0, 0, 0, 0, -1, 0, 0],
                [0, 0, 0, 0, 0, -1, 0],
                [0, 0, 0, 0, 0, 0, -1],
            ],
            date: new Date().toISOString(),
        },
        {
            name: "Simple Graph (6N)",
            data: [
                [-1, 3, 4, 0, 0, 0],
                [3, -1, 3, 5, 2, 0],
                [4, 3, -1, 0, 6, 0],
                [0, 5, 0, -1, 1, 3],
                [0, 2, 6, 1, -1, 1],
                [0, 0, 0, 3, 1, -1],
            ],
            date: new Date().toISOString(),
        },
        {
            name: "Medium Graph (8N)",
            data: [
                [-1, 450, 950, 800, 0, 0, 0, 0],
                [450, -1, 0, 0, 1800, 900, 0, 0],
                [950, 0, -1, 0, 1100, 600, 0, 0],
                [800, 0, 0, -1, 0, 600, 1200, 0],
                [0, 1800, 1100, 0, -1, 900, 0, 400],
                [0, 900, 600, 600, 900, -1, 1000, 1300],
                [0, 0, 0, 1200, -1, 1000, -1, 600],
                [0, 0, 0, 0, 400, 1300, 600, -1],
            ],
            date: new Date().toISOString(),
        },
    ],
    visual: {
        dialog: {
            file: false,
            treats: false,
            settings: false,
            about: false,
        },
        status: {
            metriks: "INSERT",
        },
    },
}

const reducer = (context: StoreProps, action: StoreAction): StoreProps => {
    if (process.env.NODE_ENV === "development") {
        console.log("STORE_ACT", action)
    }

    switch (action.type) {
        case ACTION.STORE.INIT:
            return { ...context, ...action.body }
        case ACTION.STORE.CURRENT:
            return { ...context, ...action.body }
        case ACTION.STORE.SAVED:
            return { ...context, ...action.body }
        case ACTION.STORE.VISUAL:
            return { ...context, ...action.body }

        default:
            throw new Error()
    }
}

export const StoreContext = createContext({
    store: initialState,
    dispatch: (action: StoreAction) => {},
})

export const StoreProvider: FunctionalComponent = ({ children }) => {
    const [initialization, setInitialization] = useState(false)
    const [store, dispatch] = useReducer<StoreProps, StoreAction>(
        reducer,
        initialState
    )

    useEffect(() => {
        const persistence: StoreProps = JSON.parse(
            localStorage.getItem("metriks-store") as string
        ) as unknown as StoreProps

        if (persistence) {
            dispatch({
                type: ACTION.STORE.INIT,
                body: persistence,
            })

            setInitialization(true)
        } else {
            localStorage.setItem("metriks-store", StorePropsSchema(store))
        }
    }, [])

    useEffect(() => {
        if (initialization) {
            localStorage.setItem("metriks-store", StorePropsSchema(store))
        }
    }, [store])

    return (
        <StoreContext.Provider value={{ store, dispatch }}>
            {children}
        </StoreContext.Provider>
    )
}
