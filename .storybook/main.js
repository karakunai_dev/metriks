module.exports = {
    stories: [
        "../src/components/*.stories.mdx",
        "../src/components/**/*.stories.@(ts|tsx)",
    ],
    addons: ["@storybook/addon-links", "@storybook/addon-essentials"],
    staticDirs: [
        {
            from: "../src/assets",
            to: "/assets",
        },
    ],
    babel: async (options) => ({
        ...options,
        plugins: [
            [
                "@babel/plugin-transform-react-jsx",
                { importSource: "preact", runtime: "automatic" },
            ],
        ],
    }),
    core: {
        builder: "webpack4",
    },
}
